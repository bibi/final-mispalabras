from django.contrib import admin
from .models import Palabras, Comentario, Enlace, Votos

admin.site.register(Palabras)
admin.site.register(Comentario)
admin.site.register(Enlace)
admin.site.register(Votos)
# Register your models here.
