from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Palabras(models.Model):
    tipo = models.CharField(max_length=64,default="Palabras")
    palabra = models.CharField(max_length=64)
    definicion = models.TextField(default="")
    imagen = models.URLField(default="")
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    rae = models.TextField(default="")
    flickr = models.URLField(default="")
    meme = models.URLField(default="")
    fecha = models.DateTimeField('publicado')


class Enlace(models.Model):
    tipo= models.CharField(max_length=64,default="Enlaces")
    palabra = models.ForeignKey(Palabras,on_delete=models.CASCADE)
    valor = models.URLField(default="")
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha = models.DateTimeField('publicado')
    descripcion = models.TextField(default="")
    imagen = models.URLField(default="")


class Comentario(models.Model):
    tipo= models.CharField(max_length=64,default="Comentarios")
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    valor = models.TextField(blank=False)
    fecha = models.DateTimeField('publicado')

class Votos(models.Model):
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    valor = models.IntegerField(default=0)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    fecha = models.DateTimeField('publicado')
