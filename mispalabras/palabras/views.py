from django.utils import timezone
import datetime
import json
import random
import urllib
from itertools import chain
from urllib.request import urlopen


import cloudscraper

from bs4 import BeautifulSoup
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core import serializers
from django.shortcuts import redirect
from .models import Palabras, Comentario, Votos, Enlace

from django.http import HttpResponse
from django.template import loader
import xml.etree.ElementTree as ET

def FindPalabra(request):
    findpalabra = ""
    guardarpalabra = False
    if request.method == "POST":
        if (request.POST['name'] == "login"):
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
        if (request.POST['name'] == "buscar_palabra"):
            findpalabra = request.POST['palabra']

        if (request.POST['name'] == "GuardarPalabra"):
            if request.user.is_authenticated:
                guardarpalabra = True
    return findpalabra, guardarpalabra


def contar_palabras():
    npalabras = Palabras.objects.all().__len__()
    lista = Palabras.objects.all().values_list('palabra')
    try:
        palabra = str(random.choice(lista)).split("'")[1]
    except IndexError:
        palabra = ""
    return npalabras, palabra


def topvoted():
    dict = {}
    lista_palabras = []
    palabras = Palabras.objects.all()
    for palabra in palabras:
        votos = Votos.objects.filter(palabra=palabra)
        nvotos = 0
        for voto in votos:
            nvotos = nvotos + voto.valor
        dict[palabra.palabra] = nvotos
    dict_sort = sorted(dict.items(),key=lambda x: x[1], reverse=True)[:10]

    for palabra in dict_sort:
        lista_palabras.append(palabra[0])
    return lista_palabras


def get_lista_pal(npag, request):
    lista = []
    palabrasbase = Palabras.objects.all().values()
    listatotal = (sorted(palabrasbase, key=lambda x: x['fecha'], reverse=True))
    for pall in listatotal:
        try:
            pal = Palabras.objects.get(palabra=pall['palabra'])
        except Palabras.DoesNotExist:
            pass
        votosbase = Votos.objects.filter(palabra=pal).values()
        nvotos = 0
        for voto in votosbase:
            nvotos += voto['valor']
        dict = {}
        dict['palabraa'] = pal.palabra
        dict['votoss'] = nvotos
        dict['definicionn'] = pal.definicion
        dict['imagenn'] = pal.imagen
        dict['usuarioo'] = pal.usuario.username
        lista.append(dict)
    return lista[(0 + (npag - 1) * 5):(5 + 5 * (npag - 1))]




def principal(request):
    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabras.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "json":
            data = serializers.serialize("json", Palabras.objects.all())
            return HttpResponse(data, content_type='text/json')
        elif format == "xmlcoment":
            data = serializers.serialize("xml", Comentario.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "jsoncoment":
            data = serializers.serialize("json", Comentario.objects.all())
            return HttpResponse(data, content_type='text/xml')

    # Esto es para guardarme el usuario
    findpalabra, guardarpalabra = FindPalabra(request)
    if (len(findpalabra) != 0):
        return redirect("/mispalabras/" + findpalabra)
    # Esto es por si es GET//otro formulario
    template = loader.get_template('inicio.html')  # obtengo la plantilla
    dict = topvoted()

    npalabras, palabra = contar_palabras()
    if (npalabras == 0):
        npaginas = 1
    else:
        npaginas = npalabras // 5

    if (npalabras % 5) != 0:
        npaginas += 1
    numpaginas = list(range(1, npaginas + 1))
    page = request.GET.get('page', 1)
    if page:
        npag = int(page)
        if npag == numpaginas[-1]:
            nnext = npag
        else:
            nnext = npag + 1

        if npag == numpaginas[0]:
            nprev = npag
        else:
            nprev = npag - 1

    listapal = get_lista_pal(npag, request)
    context = {
        "dict": dict,
        "npalabras": npalabras,
        "palabra": palabra,
        "numpag": npag,
        "nnext": nnext,
        "nprev": nprev,
        "numpaginas": numpaginas,
        "listpal": listapal

    }
    return HttpResponse(template.render(context, request))


def logout_view(request):
    logout(request)
    return redirect('/mispalabras/')


def register_view(request):
    if request.method == "GET":
        template = loader.get_template('register.html')
        dict = topvoted()
        npalabras, palabra = contar_palabras()
        context = {
            "dict": dict,
            "npalabras": npalabras,
            "palabra": palabra
        }
        return HttpResponse(template.render(context, request))
    else:
        findpalabra, guardarpalabra = FindPalabra(request)
        if (len(findpalabra) != 0):
            return redirect("/mispalabras/" + findpalabra)

        username = request.POST['username']
        try:
            user = User.objects.get(username=username)
            template = loader.get_template('register.html')
            dict = topvoted()
            context = {
                "dict": dict
            }
        except User.DoesNotExist:
            password = request.POST['password']
            email = request.POST['email']
            user = User.objects.create_user(username, email, password)
            login(request, user)
            return redirect('/mispalabras/')
        return HttpResponse(template.render(context, request))


def ayuda(request):
    findpalabra, guardarpalabra = FindPalabra(request)
    #Con esto soluciono el post de login y palabra que están siempre
    if (len(findpalabra) != 0):
        return redirect("/mispalabras/" + findpalabra)

    template = loader.get_template('ayuda.html')
    dict = topvoted()
    npalabras, palabra = contar_palabras()
    context = {
        "dict": dict,
        "npalabras": npalabras,
        "palabra": palabra
    }
    return HttpResponse(template.render(context, request))


def mipagina(request):
    #Con esto gestiono los post que hay siempre
    findpalabra, guardarpalabra = FindPalabra(request)
    if (len(findpalabra) != 0):
        return redirect("/mispalabras/" + findpalabra)

    template = loader.get_template('mipagina.html')
    votosbase = Votos.objects.filter(usuario=request.user).values()
    palabrasbase = Palabras.objects.filter(usuario=request.user).values()
    comentariosbase = Comentario.objects.filter(usuario=request.user).values()
    enlacesbase = Enlace.objects.filter(usuario=request.user).values()
    listatotal = list(chain(votosbase, palabrasbase, comentariosbase, enlacesbase))
    listatotal = (sorted(listatotal, key=lambda x: x['fecha'], reverse=True))

    dict = topvoted()
    npalabras, palabra = contar_palabras()

    context = {
        "listatotal": listatotal,
        "dict": dict,
        "npalabras": npalabras,
        "palabra": palabra
    }
    return HttpResponse(template.render(context, request))


def BuscaImagen(request, palabra):
    url = "https://es.wikipedia.org/w/api.php?action=query&titles=" + palabra \
          + "&prop=pageimages&format=json&pithumbsize=200"
    query = str(urlopen(url).read().decode('utf-8'))
    objeto = json.loads(query)
    url_media = objeto['query']['pages']
    id = str(url_media).split("'")[1]
    try:
        url_imagen = url_media[id]['thumbnail']['source']
    except KeyError:
        url_imagen = "http://apimeme.com/meme?meme=" + "Confused-Gandalf" + "&top=Que+te+crees&bottom=que+estas+buscando"
    return url_imagen


def BuscaDefinicion(request, palabra):
    try:
        url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + palabra \
              + "&prop=extracts&exintro&explaintext"
        doc = ET.parse(urlopen(url))
        text = doc.find('query/pages/page/extract').text
        if (text == None):
            text = "Esta palabra no está en Wikipedia en español"
    except:
        text = "Esta palabra no está en Wikipedia en español"
    return text


def BuscaDRAE(request, palabra):
    url = "https://dle.rae.es/" + palabra
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    definicion = soup.find("meta", {"property": "og:description"})
    if not definicion["content"].startswith("1."):
        return "Esta palabra no está en la DRAE"
    return definicion["content"]


def BuscaFlickr(request, palabra):
    try:
        url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + palabra

        scraper = cloudscraper.create_scraper()
        soup = BeautifulSoup(scraper.get(url).text, 'html.parser')
        urlimagen = soup.find('feed').findAll('entry')[0]
        for a in urlimagen.findAll('link', {'rel': 'enclosure'}, href=True):
            link = a['href']
    except:
        link = "http://apimeme.com/meme?meme=" + "Confused-Gandalf" + "&top=Que+te+crees&bottom=que+estas+buscando"
    return link


def delete_view(request):
    usuario = request.user
    logout(request)
    usuario.delete()
    return redirect('/mispalabras/')


def get_embebida(url):
    try:
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')
        descr = soup.find("meta", {"property": "og:description"})["content"]
        img = soup.find("meta", {"property": "og:image"})["content"]
        if (descr is None):
            descr = soup.find("meta", {"property": "og:title"})["content"]
            if descr is None:
                descr = ""
        if (img is None):
            img = ""
    except:
        return "",""
    return descr, img


def SetInfoInPalabra(request, contenido, palabra):
    if request.method == "POST":
        if (request.POST['name'] == "meme"):
            meme = ""
            texto = ""
            opcion = request.POST['opcionmeme']
            if opcion == "1":
                meme = "Afraid-To-Ask-Andy"
            elif opcion == "2":
                meme = "Advice-Dog"
            elif opcion == "3":
                meme = "Angry-Baby"
            else:
                meme = "Blob"
            texto = request.POST['texto']
            contenido.meme = "http://apimeme.com/meme?meme=" + meme + "&top=" + contenido.palabra + "&bottom=" + texto
            contenido.save()

        if request.POST['name'] == "DRAE":
            contenido.rae = BuscaDRAE(request, palabra)
            #  contenido.haverae = True
            contenido.save()

        if request.POST['name'] == "Flick":
            #  contenido.haveflickr = True
            contenido.flickr = BuscaFlickr(request, palabra)
            contenido.save()
        if request.user.is_authenticated:
            if request.POST['name'] == "comentario":
                valor = request.POST['valor']
                c = Comentario(palabra=contenido, usuario=request.user, valor=valor, fecha=datetime.datetime.now(tz=timezone.utc))
                c.save()
            if request.POST['name'] == "enlace":
                valor = request.POST['valor']
                [descr, img] = get_embebida(valor)
                c = Enlace(palabra=contenido, descripcion=descr, imagen=img, usuario=request.user, valor=valor,
                           fecha=datetime.datetime.now(tz=timezone.utc))
                c.save()
            if request.POST['name'] == "like":
                try:
                    voto = Votos.objects.get(palabra=contenido, usuario=request.user)
                    voto.valor = 1
                    voto.save()
                except Votos.DoesNotExist:
                    c = Votos(palabra=contenido, usuario=request.user, valor=1, fecha=datetime.datetime.now(tz=timezone.utc))
                    c.save()

            if request.POST['name'] == "dislike":
                try:
                    voto = Votos.objects.get(palabra=contenido, usuario=request.user)
                    voto.valor = -1
                    voto.save()
                except Votos.DoesNotExist:
                    c = Votos(palabra=contenido, usuario=request.user, valor=-1, fecha=datetime.datetime.now(tz=timezone.utc))
                    c.save()

            if request.POST['name'] == "remove":
                try:
                    voto = Votos.objects.get(palabra=contenido, usuario=request.user)
                    if voto.valor == 1:
                        voto.valor = voto.valor - 1
                    elif voto.valor == -1:
                        voto.valor = voto.valor + 1
                    voto.save()
                except Votos.DoesNotExist:
                    pass

def palabraid(request):
    id = request.GET.get('id', None)
    try:
        palabra = Palabras.objects.get(id=id).palabra
    except:
        palabra=""
    return redirect("/mispalabras/"+palabra)



def palabras(request, palabra):
    url_DRAE = ""
    url_definicion = ""
    url_imagen = ""
    url_flickr = ""
    meme = None
    showlike = True
    nvotos = 0
    comentarios_list = ""
    enlace_list = ""
    have_rae = False
    have_flickr = False
    have_meme = False

    findpalabra, guardarpalabra = FindPalabra(request)
    if (len(findpalabra) != 0):
        return redirect("/mispalabras/" + findpalabra)

    # Meter aqui el guardar palabra

    if (guardarpalabra):
        try:
            contenido = Palabras.objects.get(palabra=palabra)

        except Palabras.DoesNotExist:
            contenido = Palabras(palabra=palabra, usuario=request.user, fecha=datetime.datetime.now(tz=timezone.utc))
            contenido.definicion = BuscaDefinicion(request, palabra)
            contenido.imagen = BuscaImagen(request, palabra)
            contenido.save()
    else:  # Este es cuando veo la primera vez la pagina
        url_definicion = BuscaDefinicion(request, palabra)
        url_imagen = BuscaImagen(request, palabra)

    # Si la palabra existe => Busco si me han hecho un guardarrae etc

    try:
        contenido = Palabras.objects.get(palabra=palabra)
        pal_exist = True;
        SetInfoInPalabra(request, contenido, palabra)
        url_definicion = contenido.definicion
        url_imagen = contenido.imagen
        # Para buscar xml y json de la palabra
        format = request.GET.get('format', None)
        if format:
            if format == "xml":
                data = serializers.serialize("xml", Palabras.objects.filter(palabra=palabra))
                return HttpResponse(data, content_type='text/xml')
            elif format == "json":
                data = serializers.serialize("json", Palabras.objects.filter(palabra=palabra))
                return HttpResponse(data, content_type='text/json')


        # Busco la definicion y la imagen porque eso tengo que mostrarlo siempre

        if (len(contenido.rae) != 0):
            have_rae = True;
            url_DRAE = contenido.rae
        if (len(contenido.flickr) != 0):
            have_flickr = True;
            url_flickr = contenido.flickr
        if (len(contenido.meme) != 0):
            have_meme = True;
            meme = contenido.meme

        try:
            comentarios_list = Comentario.objects.filter(palabra=contenido)
        except Comentario.DoesNotExist:
            pass

        try:
            enlace_list = Enlace.objects.filter(palabra=contenido)
        except Enlace.DoesNotExist:
            pass
        # Esto es para ver si le enseño al usuario el boton de like/dislike
        if request.user.is_authenticated:
            try:
                voto = Votos.objects.get(palabra=contenido, usuario=request.user)
                if voto.valor != 0:
                    showlike = False
            except Votos.DoesNotExist:
                pass

        # Esto es para contar el número de votos
        votosbase = Votos.objects.filter(palabra=contenido)
        nvotos = 0
        for voto in votosbase:
            nvotos = nvotos + voto.valor



    except Palabras.DoesNotExist:
        pal_exist = False

    template = loader.get_template('palabra.html')
    dict = topvoted()
    npalabras, palabraa = contar_palabras()

    context = {
        "dict": dict,
        "thispalabra":palabra,
        "pal_exist": pal_exist,
        "URL_imagen": url_imagen,
        "URL_definicion": url_definicion,
        "URL_DRAE": url_DRAE,
        "URL_FLICKR": url_flickr,
        "URL_MEME": meme,
        "haveDRAE": have_rae,
        "haveFlickr": have_flickr,
        "have_meme": have_meme,
        "comentarios_list": comentarios_list,
        "enlace_list": enlace_list,
        "nvotos": nvotos,
        "showlike": showlike,
        "npalabras": npalabras,
        "palabra": palabraa

    }
    return HttpResponse(template.render(context, request))
