
# Create your tests here.
from django.contrib.auth.models import User
from django.test import TestCase
from django.test import Client
from .models import Palabras, Comentario, Votos, Enlace
import datetime
from django.utils import timezone


#Empezamos con inicio
class inicioTest(TestCase):
    #Get de inicio, buscando la paginación, que es característico suyo.
    def test_get_inicio(self):
        c = Client()
        response = c.get('/mispalabras/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<ul class="pagination">', content)
        self.assertNotIn('Inicio',content)

    #POST en inicio de login (aunque se puede hacer en cualquiera)
    def test_post_palabra(self):
        c = Client()
        response = c.post('/mispalabras/', {'palabra': "sol",'name':"buscar_palabra"})
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('sol', content)

    #Debes tener un superuser admin admin para que funcione
    #Si funciona el post, te devuelve a mis palabras
    def test_post_login(self):
        c = Client()
        response = c.post('/mispalabras/', {'username': "admin", 'password':"admin", 'name':"login"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<ul class="pagination">', content)


class registerTest(TestCase):
    def test_get_register(self):
        c = Client()
        response = c.get('/mispalabras/register')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Regístrate',content)

    def test_post_register(self):
        c = Client()
        response = c.post('/mispalabras/register', {'username': "bibi", 'password': "bibi", 'email': "bibi%40gmail.com", 'name':"register"})
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('/mispalabras/', content)


class deleteTest(TestCase):
    def test_get_delete(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('/mispalabras/delete')
        self.assertEqual(response.status_code, 302) #porque redirijo a mispalabras
        content = response.headers['Location']
        self.assertIn('/mispalabras/', content)

class ayudaTest(TestCase):
    def test_get_ayuda(self):
        c = Client()
        response = c.get('/mispalabras/ayuda')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3> Información sobre la web MisPalabras</h3>\n',content)
        self.assertNotIn('Ayuda',content)


class logoutTest(TestCase):
    def test_get_logout(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('/mispalabras/logout')
        self.assertEqual(response.status_code, 302) #porque redirijo a mispalabras
        content = response.headers['Location']
        self.assertIn('/mispalabras/', content)


#Para probar antes necesitamos guardar alguna informacion
class paginaTest(TestCase):
    def test_get_mipagina(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.post('/mispalabras/sol',{'name':'GuardarPalabra'})
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/mispalabras/mipagina')
        content = response.content.decode('utf-8')
        self.assertIn('sol',content)
        self.assertNotIn('Mi Pagina',content)


#Para ver si estoy loggeado
class test(TestCase):
    def test(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('/mispalabras/sol')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Logout', content)

class palabranoexisteTests(TestCase):

    def setUp(self):
        c = Client()
        c.post('/mispalabras/register', {'username': "testuser", 'password': "testuser", 'email': "testuser%40gmail.es",
                                         'name': "register"})


    def test_GETpalabra(self):
        c = Client()
        response = c.get('/mispalabras/sol')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Helios', content)

    def test_POSTpalabra(self):
        c = Client()
        response = c.post('/mispalabras/', {'palabra': "luz", 'name': "buscar_palabra"})
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('luz', content)

        response = c.get('/mispalabras/luz')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('luz', content)


# Guardamos la palabra para que ya exista
class palabraexisteTests(TestCase):
    def setUp(self):
        user = User.objects.create_user("admin", "admin@admin.es", "admin")
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        c = Palabras(palabra="luna", definicion="Nuestra luna", imagen="www.luna.es", usuario=user, fecha=datetime.datetime.now(tz=timezone.utc))
        c.save()

    def test_GETpalabra(self):
        c = Client()
        response = c.get('/mispalabras/luna')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('comentario', content)

    def test_POSTlike(self):
        response = self.client.post('/mispalabras/luna', {'name': "like"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Remove', content)
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(palabra="luna")
        votos = Votos.objects.get(palabra=c, usuario=user)
        self.assertEqual(1, votos.valor)

    def test_POSTdislike(self):
        response = self.client.post('/mispalabras/luna', {'name': "dislike"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Remove', content)
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(palabra="luna")
        voto = Votos.objects.get(palabra=c, usuario=user)
        self.assertEqual(-1, voto.valor)


    def test_POSTRAE(self):
        response = self.client.post('/mispalabras/luna', {'name': "DRAE"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Definición DRAE', content)

    def test_POSTflickr(self):
        response = self.client.post('/mispalabras/luna', {'name': "Flick"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Imagen Flickr', content)

    def test_POSTenlace(self):
        response = self.client.post('/mispalabras/luna', {'name': "enlace", 'valor': "https://www.elmundo.es"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('https://www.elmundo.es', content)
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(palabra="luna")
        enlace = Enlace.objects.get(palabra=c, usuario=user)
        self.assertEqual("https://www.elmundo.es", enlace.valor)

    def test_POSTcomentario(self):
        response = self.client.post('/mispalabras/luna', {'name': "comentario", 'valor': "Comentario de prueba"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Comentario de prueba', content)
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(palabra="luna")
        comentario = Comentario.objects.get(palabra=c, usuario=user)
        self.assertEqual("Comentario de prueba", comentario.valor)

    def test_POSTmeme(self):
        response = self.client.post('/mispalabras/luna', {'name': "meme", 'texto': "Meme prueba", 'opcionmeme': 1})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('El meme actual es:', content)